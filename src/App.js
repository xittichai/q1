import { useEffect, useState } from 'react';
export default function App() {
    const [value, setValue] = useState(null),
        [method, setMethod] = useState('isPrime'),
        [result, setResult] = useState(null);
    const correctValue = () => setValue(value < 0 ? 1 : Math.round(value));
    const determineValue = () => {
        switch (method) {
            case 'isPrime':
                if (value < 2) {
                    setResult('false');
                    break;
                }
                let isPrime = true;
                for (let i = 2; i <= value - 1; i++)
                    if (value % i === 0) {
                        isPrime = false;
                        break;
                    }
                setResult(isPrime ? 'true' : 'false');
                break;
            case 'isFibonacci':
                const isSquare = value => value > 0 && Math.sqrt(value) % 1 === 0;
                if (isSquare(5 * (value * value) - 4) || isSquare(5 * (value * value) + 4)) {
                    setResult('true');
                } else {
                    setResult('false');
                }
                break;
            default:
                setResult(null);
        }
    };
    useEffect(() => determineValue(), [value, method]);
    return (
        <div className='container'>
            <div className='leftPanel'>
                <input min={0} onBlur={() => correctValue()} onChange={e => setValue(e.target.value)} type='number' value={value} />
            </div>
            <div className='midPanel'>
                <select onChange={e => setMethod(e.target.value)}>
                    <option selected value='isPrime'>
                        isPrime
                    </option>
                    <option value='isFibonacci'>isFibonacci</option>
                </select>
            </div>
            <div className='rightPanel'>{result}</div>
        </div>
    );
}